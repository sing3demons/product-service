const { Router } = require('express')
const router = Router()

const { Types } = require('mongoose')

const Product = require('../models/product')
const { checkAuth, checkAdmin } = require('../middleware/passport')

router.get('/', checkAuth, async (req, res) => {
  const products = await Product.find({})
  res.status(200).json({ products })
})

router.get('/:id', checkAuth, async (req, res) => {
  const { id } = req.params

  if (Types.ObjectId.isValid(id)) {
    const product = await Product.findById(id)
    return res.status(200).json({ product })
  }

  const product = await Product.findOne({ product_id: id })
  res.status(200).json({ product })
})

router.post('/', [checkAuth, checkAdmin], async (req, res) => {
  try {
    const { user_id } = req.user
    const { product_id, product_name, product_price } = req.body

    const product = await Product.create({ product_id, product_name, product_price, user_id })

    res.status(201).json({ product })
  } catch (error) {
    res.status(400).json({ error })
  }
})

module.exports = router
