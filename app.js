const express = require('express')
require('dotenv').config()
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose')

const ConnectDB = async () => await mongoose.connect(process.env.MONGODB_URI)

const indexRouter = require('./routes/index')

const app = express()
ConnectDB()
  .then(() => console.log('MongoDB connected'))
  .catch((err) => console.error(err))

app.use(cors())

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
// app.use(cookieParser())

app.use('/', indexRouter)
app.use('/api/v1/product', require('./routes/product'))

module.exports = app
