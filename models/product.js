const { Schema, model, Types } = require('mongoose')

const ProductSchema = new Schema(
  {
    product_id: {
      type: Number,
      unique: true,
      required: true,
      index: true,
    },
    product_name: { type: String },
    product_price: { type: Types.Decimal128 },
    user_id: { type: String },
    created_at: { type: Date, default: Date.now },
  },
  {
    timestamps: false,
    collection: 'products',
  }
)

module.exports = model('Product', ProductSchema)
